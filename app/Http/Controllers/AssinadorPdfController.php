<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AssinadorPdfController extends Controller
{

    // Token Authorization de autenticação gerado no BRy Cloud.
    const token = "INSIRA O TOKEN JWT AQUI";
    // URLs de inicialização e finalização de assinaturas. Para testar em produção, basta remover o '.hom'
    const urlInicializacao = "https://hub2.hom.bry.com.br/fw/v1/pdf/pkcs1/assinaturas/acoes/inicializar";
    const urlFinalizacao = "https://hub2.hom.bry.com.br/fw/v1/pdf/pkcs1/assinaturas/acoes/finalizar";

    // Função chamada na inicialização da assinatura (Recebe como parametro a requisição enviada pelo front)
    public function inicializar(Request $request)
    {
        // Verifica se o documento enviado na requisição é válido.
        if ($request->hasFile('documento') && $request->file('documento')->isValid()) {
            $upload = $request->documento->storeAs('documentos', 'documentoParaAssinatura.pdf');
        } else {
            return response()->json(['message' => 'Arquivo enviado inválido'], 400);
        }
        // Verifica se a imagem enviada na requsição é valida.
        if ($request->incluirIMG === 'true') {
            if ($request->hasFile('imagem') && $request->file('imagem')->isValid()) {
                $upload = $request->imagem->storeAs('imagem', 'imagemAssinatura.png');
            } else {
                return response()->json(['message' => 'Imagem enviada inválida'], 400);
            }
        }

        $data = array(
            'dados_inicializar' =>
            '{
            "perfil" : "' . $request->perfil . '",
            "algoritmoHash" : "' . $request->algoritmoHash . '",
            "formatoDadosEntrada" : "Base64",
            "formatoDadosSaida" : "Base64",
            "certificado" : "' . $request->certificado . '",
            "nonces": ["1"]
        }',
            'documento' => new \CURLFILE(storage_path() . '\app\documentos\documentoParaAssinatura.pdf'),
    );
        if($request->assinaturaVisivel === 'true') {
            $data['configuracao_imagem'] = '{
                "altura" : "' . $request->altura . '",
                "largura" : "' . $request->largura . '",
                "coordenadaX" : "' . $request->coordenadaX . '",
                "coordenadaY" : "' . $request->coordenadaY . '",
                "posicao" : "' . $request->posicao . '",
                "pagina" : "' . $request->pagina . '"
            }';

            $data['configuracao_texto'] = '{
                "texto" : "' . $request->texto . '",
                "incluirCN" : "' . $request->incluirCN . '",
                "incluirCPF" : "' . $request->incluirCPF . '",
                "incluirEmail" : "' . $request->incluirEmail . '"
            }';

                    // Configurações da imagem da assinatura
        if ($request->incluirIMG === 'true') {
            $data['imagem'] = new \CURLFILE(storage_path() . '\app\imagem\imagemAssinatura.png');
        };
        }

        // Cria o CURL que será enviada à API de Assinatura.
        $curl = curl_init(self::urlInicializacao);

        curl_setopt_array($curl, array(
            CURLOPT_URL => self::urlInicializacao,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                'Authorization: ' . self::token,
            ),
        ));
        
        // Envia a requisição para a API de Assinatura BRy
        $respostaInicializacao = curl_exec($curl);
        if ($respostaInicializacao === false) {
            error_log('cURL Error: ' . curl_error($curl));
            return('erro no backend ao fazer a request para a api externa');
        } else {
            error_log('sucesso ao fazer request para api externa');
            error_log($respostaInicializacao);
        }
        // Recebe o código HTTP da requisição
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        // Transforma a resposta da requisição em JSON
        $respostaJson = json_decode($respostaInicializacao);
        // Encerra a requisição
        curl_close($curl);

        // Se a requisição retornou o código HTTP 200, ou seja, sucesso, ele retorna 200 para o front.
        // Caso não tenha retornado 200, retorna a resposta vindo da API e o código http de erro para o front.
        // A resposta da inicialização será usada no frontend para cifrar os dados usando a BRy Extension.
        if ($httpcode == 200) {
            error_log('resposta 200');
            return response()->json($respostaJson);
        } else {
            error_log('resposta 400');
            return response()->json($respostaJson, $httpcode);
        }
    }

    // Função chamada na finalização da assinatura (Recebe como parametro a requisição enviada pelo front)
    public function finalizar(Request $request)
    {
        // // Cria o CURL que será enviada à API de Assinatura.
        $curl = curl_init(self::urlFinalizacao);

        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>
            "{
                \"nonce\": \"$request->nonce\",
                \"formatoDeDados\": \"Base64\",
                \"assinaturasPkcs1\":
                    [
                        {
                            \"cifrado\": \"$request->cifrado\",
                            \"nonce\": \"$request->insideNonce\"
                        }
                    ]
            }",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
            ),
        ));
        //error_log(var_export($data, true));
        // Envia a requisição para a API de Assinatura BRy.
        error_log('pre request finalizaçao');
        $respostaFinalizacao = curl_exec($curl);
        error_log('pos request finalizaçao');

        if ($respostaFinalizacao === false) {
            error_log('cURL Error: ' . curl_error($curl));
            return('erro no backend ao fazer a request para a api externa');
        } else {
            error_log('sucesso ao fazer request para api externa');
            error_log($respostaFinalizacao);
        }

        // Recebe o código HTTP da requisição.
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        // Transforma a resposta da requisição em JSON.
        $respostaJson = json_decode($respostaFinalizacao);
        // Encerra a requisição.
        curl_close($curl);

        // // Se a requisição retornou o código HTTP 200, ou seja, sucesso, ele retorna 200 para o front junto
        // // com o link para download do diploma assinado.
        // // Caso não tenha retornado 200, retorna a resposta vindo da API e o código http de erro para o front.
        // return response()->json($respostaJson);
        if ($httpcode == 200) {
            return response()->json($respostaJson);
        } else {
            return response()->json($respostaJson, $httpcode);
        }
    }
}
