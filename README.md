# API de Geração de Assinatura de PDF com a BRy Extension

Este é exemplo backend de integração dos serviços da API de assinatura com clientes baseados em tecnologia Laravel (PHP), que utilizam a BRy Extension. 

Este exemplo apresenta os passos necessários para a geração de assinatura de PDF utilizando-se do certificado instalado no computador.
  - Passo 1: Recebimento do conteúdo do certificado digital e do documento a ser assinado.
  - Passo 2: Inicialização da assinatura e produção dos artefatos assinaturasInicializadas.
  - Passo 3: Envio dos dados para cifragem no frontend.
  - Passo 4: Recebimento dos dados cifrados no frontend.
  - Passo 5: Finalização da assinatura e obtenção do artefato assinado.

### Tech

O exemplo utiliza das bibliotecas PHP abaixo:
* [PHP 7.4] - PHP is a popular general-purpose scripting language that is especially suited to web development.
* [Laravel] - The PHP Framework for Web Artisans.
* [CURL] - Client URL Library.

**Observação**

Esta API em Laravel deve ser executada juntamente com o FrontEnd, desenvolvido em [React](https://gitlab.com/brytecnologia-team/integracao/api-assinatura/react/assinatura-pdf-extensao), para que seja feita a cifragem dos dados com a extensão para web "BRy Extension"

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastre-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

Além disso, no processo de geração de assinatura é obrigatório a posse de um certificado digital que identifica o autor do artefato assinado que será produzido.

Por esse motivo, é necessário configurar a BRy Extension no seu browser, assim como ter um certificado importado.

**Observação**

É necessário ter o [Laravel] e todas suas dependências instalados na sua máquina, além do [Composer] para a instalação das dependências e execução do projeto. Também é necessário executar esta aplicação juntamente com o frontend.

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente para a produção da assinatura.

| Variável  |                   Descrição                   | Arquivo de Configuração |  
| --------- | --------------------------------------------- | ----------------------- |
| token | Access Token para o consumo do serviço (JWT). |     AssinadorPdfController.php    |

## Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.

Os elementos que protegem as informações do arquivo são duas chaves de criptografia, uma pública e a outra privada. Sendo estes elementos obrigatórios para a execução deste exemplo.

**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).  

### Uso

Para execução da aplicação de exemplo, importe o projeto em sua IDE de preferência e instale as dependências. Utilizamos o [Composer] para a instalação das dependências.

Comandos:

Instalar dependências:

    -composer install 

Executar programa:

    -php artisan serve --port=3333
    

  [PHP 7.4]: <https://www.php.net/releases/7_4_0.php>
  [Laravel]: <https://laravel.com/>
  [CURL]: <https://www.php.net/manual/pt_BR/book.curl.php>
  [Composer]: <https://getcomposer.org/>
